<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class att_report_model extends CI_Model {

	public function __construct()
	{
		$this->load->library('session');
	}
	public function getStudent($data){ 
		 $where = '';
		 if($data['classId']){
		 	 $where = "att.classId=".$data['classId'];
		 }
		 if($data['month']){
 			 $where .= ' AND MONTH(att.date)='.$data['month'];
		 }
		 if($data['year']){
 			 $where .= ' AND YEAR(att.date)='.$data['year'];
		 } 

		 $this->db->select('*');
		 $this->db->from('studentinfo as info'); 
		 $this->db->join('studentdetails as detail', 'detail.Id=info.StdDetailsId');
		 $this->db->join('attendance as att', 'att.classId=info.StdClassId AND att.rollNo=info.StdRollNo');
		
		 $this->db->where($where);
		return $this->db->get();
		  
	}

	
}