<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attendance extends CI_Controller {
	
	public function __construct(){
		parent:: __construct();
		$this->load->model('attendance_model');
	} 
	
	public function index(){
		if($this->input->post()){

			$data['page_title'] = 'Students Attendance:: School management system';
			$data['student']= @$this->attendance_model->getStudent($this->input->post());
			$this->load->view('include/header',$data);
			$this->load->view('attendance/index');
			$this->load->view('include/footer');
		}else{
			$data['page_title'] = 'Students Attendance:: School management system';
			$this->load->view('include/header',$data);
			$this->load->view('attendance/index');
			$this->load->view('include/footer');
		}
				
	}
	public function create(){
		$data['page_title'] = 'Create Student :: School management system';
		$this->load->view('include/header',$data);
		$this->load->view('students/create');
		$this->load->view('include/footer');
	}
	
	public function insert(){
	 
		$roll = $this->input->post('roll');
		$attendace = $this->input->post('attendance');
		$date = $this->input->post('date');
		$class = $this->input->post('class');
		if(@$this->input->post('SectionId')){
			$sectionId = $this->input->post('SectionId');
		}else{
			$sectionId = '';
		}
		foreach($roll as $key=>$value){
			if(@$attendace[$key]){
				$data = array(
					'date'=>$date,
					'classId'=>$class,
					'rollNo'=>$key,
					'sectionId'=>$sectionId,
					'status'=>1
				);
				 
			}else{
				$data = array(
					'date'=>$date,
					'classId'=>$class,
					'rollNo'=>$key,
					'status'=>0
				);
				
				 $find_array =  array(
					'StdRollNo' => $data['rollNo'], 
					'StdClassId' => $data['classId']
				); 
				if($data['sectionId']){
					$find_array['StdSectionId']= $data['sectionId'];
					$data['sectionId']=$sectionId;
				}
				$std = $this->db->get_where('studentinfo', $find_array);
				echo $this->db->last_query();
				$mobile_no = $this->db->get_where('studentdetails',array('id'=>$std->row()->StdDetailsId))->row()->StdContactNo;
				#call sms method
				$sms_data = array(
					'receipent_no'=>$mobile_no,
					'sms'=>'your child is not present in the class!'
				);
				$this->send_sms($sms_data);
			}
			$this->db->insert('attendance',$data);
		}
		 redirect('attendance/index');
	}
	 
	private function send_sms($sms_data){
		# send sms
		 
	}
	 
	public function getabsence(){
		if($this->input->post()){
			$post_data = $this->input->post();
			$data['student']= @$this->attendance_model->getAbsenceStudent($post_data);
			
		}else{

		}
			$data['page_title'] = 'Students Attendance:: School management system';	 
		 	#get all classes:
		 	$data['classes'] = $this->db->get_where('classes', array('ClassStatus'=>1))->result();
						  
			$this->load->view('include/header',$data);
			$this->load->view('attendance/getabsence');
			$this->load->view('include/footer');
	}
}	