<div class="page-content">
	<div class="row-fluid">
		<div class="span12 page-header position-relative">
			<!--PAGE CONTENT BEGINS-->
				<a href="<?php echo base_url();?>classes/index">
					<button class="btn btn-primary pull-right">
											<i class="icon-cogs bigger-125"></i>
											Manage SMS Templates
					</button>
				</a>	
					 
						<h1>
						<i class="icon-hand-right icon-animated-hand-pointer blue"></i>
							Create New SMS Template
							<small>
								<i class="icon-double-angle-right"></i>
								Please Provide the following informations: 
							</small>
						</h1>
					 </div>
					<div class="row-fluid">
						<div class="span12">
						
						<!--------------Message---------------------------------->
						<!--check any alert message or not -->
						 <?php
						 	if($this->session->flashdata('status_right')):
							
						 ?>
						 <!--Print Success Alert Message: -->
								
								<div class="alert alert-success no-margin">
									<button type="button" class="close" data-dismiss="alert">
										<i class="icon-remove red"></i>
									</button>
								
									<i class="icon-ok bigger-120 blue"></i>
									<?php echo $this->session->flashdata('status_right'); ?>
								</div>
						<?php endif; ?>
						<!--check any alert message or not -->
						 <?php
						 	if($this->session->flashdata('status_wrong')):
							
						 ?>
						 <!--Print Wrong Alert Message: -->		
								<div class="alert span12 alert-danger no-margin">
									<button type="button" class="close" data-dismiss="alert">
										<i class="icon-remove red"></i>
									</button>
								
									<div class="span1"><i class="icon-warning-sign icon-2x red"></i></div>
									<div class="span6"><?php echo $this->session->flashdata('status_wrong'); ?></div>
								</div>
							<?php endif; ?>
						<!--------------Message---------------------------------->
							<div class="control-group">
								<label class="control-label" for="class_name">Template Name</label>
							<?php echo form_open(base_url().'sms/insert_template','class="form-horizontal"'); 
								
								$tempName = array(
									'name'			=> 'name',
									'id'			=>'name',
									'placeholder'	=> 'Enter sms template name',
									'value'			=> @$this->input->post('name')
								);
								
								echo form_input($tempName);
								echo '<br/>'.form_error('name', '&nbsp;&nbsp;<span class="text-warning orange"><i class="icon-warning-sign"></i>&nbsp;', '</span>');
							
								$tempText = array(
									'name'		=>'text',
									'id'		=>'text',
									'placeholder'	=> 'Enter sms templte text',
									'value'			=> @$this->input->post('text')
								);
								echo '<br/>'.form_label('SMS text');
								echo form_textarea($tempText);
								echo '<br/>'.form_error('text', '&nbsp;&nbsp;<span class="text-warning orange"><i class="icon-warning-sign"></i>&nbsp;', '</span>');
								
							?>
							
							 </div>
							 
						 
							 
							 <div class="control-group">
								   <label class="control-label" for="status">Template Status</label>
								 
								 <?php 
											
										 
										echo form_dropdown('status',array('1'=>'active','0'=>'inactive'), @$this->input->post('ClassStatus') ? @$this->input->post('status') : 1 );
										echo '<br/>'.form_error('status', '&nbsp;&nbsp;<span class="text-warning orange"><i class="icon-warning-sign"></i>&nbsp;', '</span>');
								 ?>
							</div> 
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				
					
					 
							<button class="btn btn-info" type="submit">
								<i class="icon-ok bigger-110"></i>
								Add this class
							</button>

							&nbsp; &nbsp; &nbsp;
							<button class="btn" type="reset">
								<i class="icon-undo bigger-110"></i>
								Reset
							</button>
						  
							<?php echo form_close(); ?>
				
			<!--PAGE CONTENT ENDS-->
		</div><!--/.span-->
	</div><!--/.row-fluid-->
</div><!--/.page-content-->
<script type="text/javascript">
$( document ).ready(function() {
  
$('#haveAnySection').click(function() {
    if($("#haveAnySection").is(':checked')){
    	$("#section").removeClass('hide');   
	    $("#teacher").addClass('hide');
    }
	       
	else{
		$("#section").addClass('hide');
	     $("#teacher").removeClass('hide');
	}
	     
	});
});


</script>