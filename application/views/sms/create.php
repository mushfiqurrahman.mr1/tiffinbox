<div class="page-content">
	<div class="row-fluid">
		<div class="span12 page-header position-relative">
			<!--PAGE CONTENT BEGINS-->
				 	
					 
						<h1>
						<i class="icon-hand-right icon-animated-hand-pointer blue"></i>
							Send SMS
							<small>
								<i class="icon-double-angle-right"></i>
								Please Provide the following informations: 
							</small>
						</h1>
					 </div>
					<div class="row-fluid">
						<div class="span12">
						
						<!--------------Message---------------------------------->
						<!--check any alert message or not -->
						 <?php
						 	if($this->session->flashdata('status_right')):
							
						 ?>
						 <!--Print Success Alert Message: -->
								
								<div class="alert alert-success no-margin">
									<button type="button" class="close" data-dismiss="alert">
										<i class="icon-remove red"></i>
									</button>
								
									<i class="icon-ok bigger-120 blue"></i>
									<?php echo $this->session->flashdata('status_right'); ?>
								</div>
						<?php endif; ?>
						<!--check any alert message or not -->
						 <?php
						 	if($this->session->flashdata('status_wrong')):
							
						 ?>
						 <!--Print Wrong Alert Message: -->		
								<div class="alert span12 alert-danger no-margin">
									<button type="button" class="close" data-dismiss="alert">
										<i class="icon-remove red"></i>
									</button>
								
									<div class="span1"><i class="icon-warning-sign icon-2x red"></i></div>
									<div class="span6"><?php echo $this->session->flashdata('status_wrong'); ?></div>
								</div>
							<?php endif; ?>
						<!--------------Message---------------------------------->
							<div class="control-group">
								<label class="control-label" for="class_name">Recipient</label>
							<?php echo form_open(base_url().'sms/send','class="form-horizontal"'); 
								
								$recipient = array(
									'name'			=> 'recipient',
									'id'			=>'recipient',
									'placeholder'	=> 'Enter recipient number',
									'value'			=> @$this->input->post('recipient'),
									
								);
								
								echo form_input($recipient);
								echo '<br/>'.form_error('recipient', '&nbsp;&nbsp;<span class="text-warning orange"><i class="icon-warning-sign"></i>&nbsp;', '</span>');
							
								$smsText = array(
									'name'		=>'smsMessage',
									'id'		=>'smsMessage',
									'placeholder'	=> 'Enter sms (160 characters)',
									'value'			=> @$this->input->post('smsMessage'),
									'maxlength'		=>"160"
								);
								echo '<br/>'.form_label('SMS Text');
								echo form_textarea($smsText);
								echo '<br/>'.form_error('smsText', '&nbsp;&nbsp;<span class="text-warning orange"><i class="icon-warning-sign"></i>&nbsp;', '</span>');
								
							?>
							
							 </div>
							 
							<div class="control-group">
									<label class="control-label" for="haveAnySection">
									<input name="haveAnySection" id="haveAnySection" value="1" class="ace-checkbox-2" type="checkbox">
									<span class="lbl green">&nbsp;&nbsp;Selete from Template?</span> 
									</label>

									 
							</div>
							<div id="section" class="control-group" style="display: none;">
								<label class="control-label" for="form-field-tags">
									<?php 
										$options[''] = '--select template--';
									foreach($templates->result() as $template):
										$options[$template->id] = $template->name.' :: '.$template->text;
									endforeach;
									echo form_dropdown('', $options,'', 'id="add-section"');
									?>
								</label>
							</div>
								
							 
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				
					
					 
							<button class="btn btn-info" type="submit">
								<i class="icon-ok bigger-110"></i>
								Send SMS
							</button>

							&nbsp; &nbsp; &nbsp;
							<button class="btn" type="reset">
								<i class="icon-undo bigger-110"></i>
								Reset
							</button>
						  
							<?php echo form_close(); ?>
				
			<!--PAGE CONTENT ENDS-->
		</div><!--/.span-->
	</div><!--/.row-fluid-->
</div><!--/.page-content-->
<script type="text/javascript">
$( document ).ready(function() {
  
$('#haveAnySection').click(function() {
    if($("#haveAnySection").is(':checked')){
    	$("#section").css('display','block');   
	    
    }
	       
	else{
		$("#section").css('display','none');   
	    $("#smsMessage").val(''); 
	}
	     
	});

	$("#add-section").change(function(){ 
		 var temp = $.trim($(this).find('option:selected').text().split('::')[1]);
		 $("#smsMessage").val(temp);
	});
});


</script>