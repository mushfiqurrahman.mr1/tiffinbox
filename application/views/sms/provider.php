<div class="page-content">
	<div class="row-fluid">
		<div class="span12 page-header position-relative">
			<!--PAGE CONTENT BEGINS-->
				 	
					 
						<h1>
						<i class="icon-hand-right icon-animated-hand-pointer blue"></i>
							Send SMS
							<small>
								<i class="icon-double-angle-right"></i>
								Please Provide the following informations: 
							</small>
						</h1>
					 </div>
					<div class="row-fluid">
						<div class="span12">
						
						<!--------------Message---------------------------------->
						<!--check any alert message or not -->
						 <?php
						 	if($this->session->flashdata('status_right')):
							
						 ?>
						 <!--Print Success Alert Message: -->
								
								<div class="alert alert-success no-margin">
									<button type="button" class="close" data-dismiss="alert">
										<i class="icon-remove red"></i>
									</button>
								
									<i class="icon-ok bigger-120 blue"></i>
									<?php echo $this->session->flashdata('status_right'); ?>
								</div>
						<?php endif; ?>
						<!--check any alert message or not -->
						 <?php
						 	if($this->session->flashdata('status_wrong')):
							
						 ?>
						 <!--Print Wrong Alert Message: -->		
								<div class="alert span12 alert-danger no-margin">
									<button type="button" class="close" data-dismiss="alert">
										<i class="icon-remove red"></i>
									</button>
								
									<div class="span1"><i class="icon-warning-sign icon-2x red"></i></div>
									<div class="span6"><?php echo $this->session->flashdata('status_wrong'); ?></div>
								</div>
							<?php endif; ?>
						<!--------------Message---------------------------------->
							<div class="control-group">
								<label class="control-label" for="class_name">Recipient</label>
							<?php echo form_open(base_url().'sms/send','class="form-horizontal"'); 
								
								$recipient = array(
									'name'			=> 'recipient',
									'id'			=>'recipient',
									'placeholder'	=> 'Enter recipient number',
									'value'			=> @$this->input->post('recipient'),
									
								);
								
								echo form_input($recipient);
								echo '<br/>'.form_error('recipient', '&nbsp;&nbsp;<span class="text-warning orange"><i class="icon-warning-sign"></i>&nbsp;', '</span>');
							
								$smsText = array(
									'name'		=>'smsMessage',
									'id'		=>'smsMessage',
									'placeholder'	=> 'Enter sms (160 characters)',
									'value'			=> @$this->input->post('smsMessage'),
									'maxlength'		=>"160"
								);
								echo '<br/>'.form_label('SMS Text');
								echo form_textarea($smsText);
								echo '<br/>'.form_error('smsText', '&nbsp;&nbsp;<span class="text-warning orange"><i class="icon-warning-sign"></i>&nbsp;', '</span>');
								
							?>
							
							 </div>
							 
							  
							 
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				
					
					 
							<button class="btn btn-info" type="submit">
								<i class="icon-ok bigger-110"></i>
								Send SMS
							</button>

							&nbsp; &nbsp; &nbsp;
							<button class="btn" type="reset">
								<i class="icon-undo bigger-110"></i>
								Reset
							</button>
						  
							<?php echo form_close(); ?>
				
			<!--PAGE CONTENT ENDS-->
		</div><!--/.span-->
	</div><!--/.row-fluid-->
</div><!--/.page-content-->
 